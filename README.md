# embla-evr-03

#local startup script can be found in:
/opt/start_scripts/embla-evr-03/st.iocsh

#Connecting to IOC shell
$ console <tab> <tab>

#Disconnecting from the IOC shell by typing
ctrl e c .

#Restart the IOC with the command

"> exit"
